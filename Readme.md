Error details: Row was updated or deleted by another transaction

Updating 2 media streams and 1 bandwidth:

```
Threads: 1 Run time: 10008 Requests: 890 Errors: 0 Retries: 0 Req per sec: 88
Threads: 2 Run time: 10007 Requests: 1859 Errors: 0 Retries: 4 Req per sec: 185
Threads: 4 Run time: 10007 Requests: 2799 Errors: 0 Retries: 16 Req per sec: 279
Threads: 8 Run time: 10010 Requests: 4738 Errors: 0 Retries: 76 Req per sec: 473
Threads: 16 Run time: 10025 Requests: 5734 Errors: 0 Retries: 166 Req per sec: 571
Threads: 32 Run time: 10051 Requests: 5594 Errors: 0 Retries: 156 Req per sec: 556
Threads: 64 Run time: 10102 Requests: 5497 Errors: 0 Retries: 173 Req per sec: 544
```

```
Threads: 1 Run time: 10010 Requests: 784 Errors: 0 Retries: 0 Req per sec: 78
Threads: 2 Run time: 10009 Requests: 1801 Errors: 0 Retries: 4 Req per sec: 179
Threads: 4 Run time: 10019 Requests: 3165 Errors: 0 Retries: 26 Req per sec: 315
Threads: 8 Run time: 10012 Requests: 4610 Errors: 0 Retries: 78 Req per sec: 460
Threads: 16 Run time: 10032 Requests: 5955 Errors: 0 Retries: 261 Req per sec: 593
Threads: 32 Run time: 10047 Requests: 6277 Errors: 0 Retries: 340 Req per sec: 624
Threads: 64 Run time: 10101 Requests: 6344 Errors: 0 Retries: 379 Req per sec: 628
```