package com.example.sdndemosql;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootTest
class SdnDemoSqlApplicationTests {
	Random random = new Random();

	@Autowired
	MediaStreamService mediaStreamService;

	@Test
	void createData() {
		mediaStreamService.createData();
	}

	@Test
	void updateSingle() {
		mediaStreamService.updateStream("0", "1", Arrays.asList("0", "1"));
	}

	@Test
	void updateMulti() {
		mediaStreamService.createData();

		ExecutorService executor = Executors.newCachedThreadPool();
		List<Future<Integer>> futures = new ArrayList<>();
		int threads = 64;
		for (int i = 0; i < threads; i++) {
			int id = i;
			futures.add(executor.submit(() -> clientThread(id)));
		}
		long start = System.currentTimeMillis();
		int sum = futures.stream().mapToInt(this::resolve).sum();
		long time = System.currentTimeMillis() - start;

		log.info("Threads: {} Run time: {} Requests: {} Errors: {} Retries: {} Req per sec: {}",
				threads, time, sum, errorCounter.get(), retryCounter.get(),
				(sum * 1000 / time));
	}

	@SneakyThrows
	private int resolve(Future<Integer> integerFuture) {
		return integerFuture.get();
	}

	AtomicInteger errorCounter = new AtomicInteger();
	AtomicInteger retryCounter = new AtomicInteger();

	private int clientThread(int id) {
//		log.info("Thread {} started", id);
		int counter = 0;
		long start = System.currentTimeMillis();
		while (System.currentTimeMillis() - start < 10000) {
			String source = String.valueOf(random.nextInt(1000));
			String destination = String.valueOf(random.nextInt(1000));
			List<String> links = new ArrayList<>();
			for (int i = 0; i < 5; i++) {
				links.add(String.valueOf(random.nextInt(100)));
			}
			if (updateStream(source, destination, links)) {
				counter++;
			} else {
				errorCounter.incrementAndGet();
			}
		}
//		log.info("Thread {} finished", id);
		return counter;
	}

	private boolean updateStream(String source, String destination, List<String> links) {
		for (int i = 0; i < 5; i++) {
			try {
				mediaStreamService.updateStream(source, destination, links);
				return true;
			} catch (Exception e) {
				retryCounter.incrementAndGet();
				//log.error(e.getMessage());
			}
		}
		return false;
	}
}
