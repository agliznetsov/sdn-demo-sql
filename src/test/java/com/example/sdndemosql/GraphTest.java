package com.example.sdndemosql;

import java.util.List;
import java.util.stream.Collectors;

import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.interfaces.ShortestPathAlgorithm;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.alg.shortestpath.KShortestSimplePaths;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.builder.GraphTypeBuilder;
import org.junit.jupiter.api.Test;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GraphTest {
	@Test
	void findPaths() {
		Graph<String,NamedEdge> graph = GraphTypeBuilder.<String,NamedEdge>undirected()
				.allowingMultipleEdges(true)
				.allowingSelfLoops(false)
				.edgeClass(NamedEdge.class)
				.weighted(false)
				.buildGraph();

		for (int i = 1; i <= 5; i++) {
			graph.addVertex(String.valueOf(i));
		}

		graph.addEdge("1", "3", new NamedEdge("a"));
		graph.addEdge("1", "3", new NamedEdge("b"));
		graph.addEdge("1", "4", new NamedEdge("c"));
		graph.addEdge("1", "5", new NamedEdge("d"));

		graph.addEdge("2", "3", new NamedEdge("e"));
		graph.addEdge("2", "4", new NamedEdge("f"));
		graph.addEdge("2", "5", new NamedEdge("g"));


		findPaths(graph, "3", "5");
		findPaths(graph, "5", "3");
	}

	private void findPaths(Graph<String,NamedEdge> graph, String from, String to) {
		log.info("Pathd from {} to {}", from, to);
		KShortestSimplePaths<String,NamedEdge> alg = new KShortestSimplePaths<>(graph);
		List<GraphPath<String,NamedEdge>> paths = alg.getPaths(from, to, 100);

		paths.forEach(it -> {
			String path = it.getEdgeList().stream().map(NamedEdge::getName).collect(Collectors.joining(", "));
			log.info(path);
		});
	}

	@Getter
	private static class NamedEdge extends DefaultEdge {
		private final String name;

		public NamedEdge(String name) {
			this.name = name;
		}
	}
}
