package com.example.sdndemosql;


import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class MediaStreamController {
	@Autowired
	MediaStreamService mediaStreamService;

	@PostMapping("/data")
	public void createData() {
		mediaStreamService.createData();
	}

	@PostMapping("/stream")
	public ResponseEntity<Integer> updateStream(@RequestParam("source") String source, @RequestParam("destination") String destination,
	                                   @RequestParam("links") String links) {
		int retryCount =  updateStreamWithRetry(source, destination, Arrays.asList(links.split(",")));
		if (retryCount < 0) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(-1);
		} else {
			return ResponseEntity.ok().body(retryCount);
		}
	}

	public int updateStreamWithRetry(String source, String destination, List<String> links) {
		for (int i = 0; i < 5; i++) {
			try {
				mediaStreamService.updateStream(source, destination, links);
				return i;
			} catch (Exception e) {
				log.warn(e.getMessage());
			}
		}
		return -1;
	}

}
