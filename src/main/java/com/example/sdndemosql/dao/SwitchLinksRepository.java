package com.example.sdndemosql.dao;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.sdndemosql.model.SwitchLinks;

@Repository
public interface SwitchLinksRepository extends JpaRepository<SwitchLinks,String> {
	@Lock(LockModeType.PESSIMISTIC_WRITE)
	@Query("from SwitchLinks where id = :id")
	SwitchLinks findByIdForUpdate(String id);
}
