package com.example.sdndemosql.dao;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.sdndemosql.model.MediaStream;
import com.example.sdndemosql.model.MediaStreamEndpoint;


@Repository
public interface MediaStreamRepository extends JpaRepository<MediaStream, String> {
	List<MediaStream> findBySourceOrDestination(MediaStreamEndpoint source, MediaStreamEndpoint destination);
}
