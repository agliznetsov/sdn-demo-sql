package com.example.sdndemosql;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.example.sdndemosql.dao.MediaStreamRepository;
import com.example.sdndemosql.dao.SwitchLinksRepository;
import com.example.sdndemosql.model.MediaStream;
import com.example.sdndemosql.model.MediaStreamEndpoint;
import com.example.sdndemosql.model.SwitchLinks;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@Transactional
public class MediaStreamService {
	@Autowired
	MediaStreamRepository mediaStreamRepository;
	@Autowired
	SwitchLinksRepository switchLinksRepository;

	ObjectMapper objectMapper = new ObjectMapper();


	String data;

	public MediaStreamService() {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < 1024; i++) {
			sb.append(' ');
		}
		data = sb.toString();
	}

	@SneakyThrows
	public void createData() {
		mediaStreamRepository.deleteAll();
		mediaStreamRepository.flush();
		switchLinksRepository.deleteAll();
		switchLinksRepository.flush();
		for (int i = 0; i < 1000; i++) {
			MediaStream ms = new MediaStream();
			ms.setId(UUID.randomUUID().toString());
			ms.setSource(endpoint(String.valueOf(i)));
			ms.setDestination(endpoint(String.valueOf(i + 1)));
			mediaStreamRepository.save(ms);
		}
		SwitchLinks link = new SwitchLinks();
		link.setId(SwitchLinks.ID);
		link.setBandwidth(objectMapper.writeValueAsString(new HashMap<>()));
		switchLinksRepository.save(link);
	}

	private MediaStreamEndpoint endpoint(String port) {
		return MediaStreamEndpoint.builder()
				.switchId("1").portId(port).ipAddress("123").ipPort(8080)
				.build();
	}

	@SneakyThrows
	public void updateStream(String source, String destination, List<String> links) {
		List<MediaStream> streams = mediaStreamRepository.findBySourceOrDestination(endpoint(source), endpoint(destination));
		SwitchLinks switchLinks = switchLinksRepository.findById(SwitchLinks.ID).get();

		streams.forEach(it -> it.setTime(System.currentTimeMillis()));
		Map<String,Integer> map = objectMapper.readValue(switchLinks.getBandwidth(), Map.class);
		links.forEach(k -> map.put(k, 0));
		switchLinks.setBandwidth(objectMapper.writeValueAsString(map));

		mediaStreamRepository.saveAll(streams);
		switchLinksRepository.save(switchLinks);
	}

}
