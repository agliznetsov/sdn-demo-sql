package com.example.sdndemosql.model;

import java.util.UUID;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.data.annotation.Version;

import lombok.Data;

@Data
@Entity
@Table(indexes = {
		@Index(columnList = "source_switch_id, source_port_id, source_ip_address, source_ip_port"),
		@Index(columnList = "destination_switch_id, destination_port_id, destination_ip_address, destination_ip_port", unique = true)
})
public class MediaStream {
	@Id
	private String id = UUID.randomUUID().toString();

	@Version
	private Long version;

	@Embedded
	@AttributeOverrides({
			@AttributeOverride(name = "switchId", column = @Column(name = "source_switch_id")),
			@AttributeOverride(name = "portId", column = @Column(name = "source_port_id")),
			@AttributeOverride(name = "ipAddress", column = @Column(name = "source_ip_address")),
			@AttributeOverride(name = "ipPort", column = @Column(name = "source_ip_port"))
	})
	private MediaStreamEndpoint source;

	@Embedded
	@AttributeOverrides({
			@AttributeOverride(name = "switchId", column = @Column(name = "destination_switch_id")),
			@AttributeOverride(name = "portId", column = @Column(name = "destination_port_id")),
			@AttributeOverride(name = "ipAddress", column = @Column(name = "destination_ip_address")),
			@AttributeOverride(name = "ipPort", column = @Column(name = "destination_ip_port"))
	})
	private MediaStreamEndpoint destination;

	private String groupId;

	private String deploymentId;

	private Long bandwidthInBps;

//	private String sourceIpAddress;

	private long time;

	//private List<DirectedSwitchLink> links = new ArrayList<>();
}
