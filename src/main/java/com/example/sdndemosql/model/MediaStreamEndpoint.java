package com.example.sdndemosql.model;

import javax.persistence.Embeddable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Embeddable
public class MediaStreamEndpoint {
	private String switchId;
	private String portId;
	private String ipAddress;
	private int ipPort;
}
