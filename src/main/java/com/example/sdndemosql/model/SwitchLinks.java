package com.example.sdndemosql.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Version;

import lombok.Data;

@Entity
@Data
public class SwitchLinks {
	public static final String ID = "ID";

	@Id
	String id;

	@Column(columnDefinition = "text")
	String bandwidth;

	@Version
	long version;
}
